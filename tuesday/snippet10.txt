
# PLOT IN GOOGLE EARTH
# unproject to WGS84; data assumed to be in Swedish grid

kos2d <- st_as_sf(Koski2007[,c("Locale_E", "Locale_N", "LMT_date")], 
                  coords = c("Locale_E", "Locale_N"), crs=CRS_LOCAL)
koswgs84 <- st_transform(kos2d, CRS_WGS84)

st_write(koswgs84, "testkos.kml", append=F)
