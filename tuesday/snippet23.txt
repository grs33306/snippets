lazy_dens <- kde2d(Koski2007$Locale_E[lazy], Koski2007$Locale_N[lazy], n=500,
                   lims=c(range(Koski2007$Locale_E), range(Koski2007$Locale_N)))
lazy_dens$z <- replace(lazy_dens$z, lazy_dens$z < quantile(lazy_dens$z, 0.9), NA)