# 2D INTERSECTION OF TRAJECTORIES (using spatial objects and methods from sp & rgeos libraries)
lGrivla <- Lines(Line(cbind(Grivla2007$Locale_E, Grivla2007$Locale_N)),"1")
lGrivla <- SpatialLines(list(lGrivla), proj4string = CRS("+init=epsg:2400"))
lGrivla <- SpatialLinesDataFrame(lGrivla, data=data.frame(ID="1",name="Grivla"), match.ID = T)

lKoski <- Lines(Line(cbind(Koski2007$Locale_E, Koski2007$Locale_N)),"1")
lKoski <- SpatialLines(list(lKoski), proj4string = CRS("+init=epsg:2400"))
lKoski <- SpatialLinesDataFrame(lKoski, data=data.frame(ID="1",name="Koski"), match.ID = T)

# spatial intersection, no time yet
scoin <- gIntersection(lGrivla, lKoski)

xscale <- c(min(Grivla2007$Locale_E,Koski2007$Locale_E),max(Grivla2007$Locale_E,Koski2007$Locale_E))
yscale <- c(min(Grivla2007$Locale_N,Koski2007$Locale_N),max(Grivla2007$Locale_N,Koski2007$Locale_N))

spplot(lGrivla, zcol="name",col.regions="blue", lwd=1, colorkey=FALSE,
       xlim=xscale, ylim=yscale, sp.layout=list(list("sp.lines", lKoski, col="red"),
                                                list("sp.points", scoin, pch=19, col="black")))