# 2D Plot trajectories Koski & Grivla
plot(Koski2007$Locale_E, Koski2007$Locale_N, type="l", asp=1,
     xlim=c(min(Koski2007$Locale_E, Grivla2007$Locale_E), 
            max(Koski2007$Locale_E, Grivla2007$Locale_E)),
     col="red", xlab = "x", ylab = "y")
lines(Grivla2007$Locale_E, Grivla2007$Locale_N, col="blue")
legend("topright", c("Koski", "Grivla"), lty=1, col=c("red", "blue"))