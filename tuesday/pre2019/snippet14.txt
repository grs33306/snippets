# apply the functions to the trajectories of both bears
tdataframe <- data.frame(tGriv=numeric(nrow(scoin@coords)), tKosk=numeric(nrow(scoin@coords)))
for (j in 1:nrow(scoin@coords)){
  for (ind in 2:nrow(Koski2007))
    if (OnLineSegment(scoin@coords[j,1],scoin@coords[j,2],Koski2007$Locale_E[ind-1], 
                     Koski2007$Locale_N[ind-1],Koski2007$Locale_E[ind], 
                     Koski2007$Locale_N[ind])){
      tdataframe$tKosk[j] <- FindTime(scoin@coords[j,1],scoin@coords[j,2],ind,Koski2007)
      break # once a match is found you are done
    }
  for (ind in  2:nrow(Grivla2007))
    if (OnLineSegment(scoin@coords[j,1],scoin@coords[j,2],Grivla2007$Locale_E[ind-1], 
                     Grivla2007$Locale_N[ind-1],Grivla2007$Locale_E[ind], 
                     Grivla2007$Locale_N[ind])){
      tdataframe$tGriv[j] <- FindTime(scoin@coords[j,1],scoin@coords[j,2],ind,Grivla2007)
      break # once a match is found you are done
    }
}
delta_t <- 8.0
stClose <- scoin[which(abs(tdataframe$tGriv - tdataframe$tKosk) < delta_t, arr.ind=T),]
stTimes <- apply(tdataframe[which(abs(tdataframe$tGriv - tdataframe$tKosk) < delta_t, arr.ind=T),],1,mean)