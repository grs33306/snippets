Koski2007 <- subset(data2007, PubName == "Koski (2310)")

Grivla2007 <- subset(data2007, PubName == "Grivla (2911)")

Koski2007 <- Koski2007[order(Koski2007$LMT_date),]      # order on data_time

Grivla2007 <- Grivla2007[order(Grivla2007$LMT_date),]   # order on data_time
