# potential implementation of the algorithm to label and
# delete waiting points and obvious erroneous points
clmns <- c('Locale_E', 'Locale_N', 'LMT_date')

mating2009$frSpeed <- NA
mating2009$frDist <- NA
for (name in bearnames){
  ndx <- which(mating2009$Name == name)
  ndx <- ndx[order(mating2009$LMT_date[ndx])] # order on time
  mating2009$frSpeed[ndx[1:(length(ndx)-1)]] <- 
    speed(mating2009[ndx[1:(length(ndx)-1)],clmns], 
          mating2009[ndx[2:length(ndx)],clmns])
  mating2009$frDist[ndx[1:(length(ndx)-1)]] <- 
    ppDist(mating2009[ndx[1:(length(ndx)-1)],clmns], 
          mating2009[ndx[2:length(ndx)],clmns])
}

# find index numbers of redundant points and erroneous points
delme <- which(mating2009$frDist < 25)

# add indices of points requiring impossible travel speeds
for (name in bearnames){
  ndx <- which(mating2009$Name == name)
  ndx <- ndx[order(mating2009$LMT_date[ndx])]
  delme <- append(delme,
                  ndx[which(mating2009$frSpeed[ndx[1:(length(ndx)-1)]] > 25 &
                        mating2009$frSpeed[ndx[2:length(ndx)]] > 25)+1])
  rm(ndx)
}

mating2009 <- mating2009[-delme,]