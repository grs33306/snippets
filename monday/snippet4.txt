# function for computing speed [km/h] between two points
speed <- function(pnt1, pnt2){
  dist <- ppDist(pnt1, pnt2)/1000
  tdif <- as.numeric(difftime(pnt2[[3]], pnt1[[3]], units = "hours"))
  dist/tdif
}