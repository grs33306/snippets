# 10-fold cross validation
# randomly assign 10 ~equally sized folds
# start with a regular sequence and next shuffle
traindata$fold <- rep(1:10, 17628)[1:176273]
set.seed(123456)
traindata$fold <- sample(traindata$fold)

# initiate the sums of squares to zero
SSresCV <- 0
SStotCV <- 0

# do the cross validation
for (fold in 1:10) {
  ndx <- which(traindata$fold == fold)
  treeCV  <- rpart(dens~height+slope, traindata[-ndx,])
  muDenCV <- mean(traindata$dens[ndx])
  SSresCV <- sum((traindata$dens[ndx] - predict(treeCV, traindata[ndx,]))^2) + 
    SSresCV
  SStotCV <- sum((traindata$dens[ndx] - muDenCV)^2) + SStotCV
}

# compute the goodness of fit metric
R2CV <- 1 - SSresCV/SStotCV
R2CV