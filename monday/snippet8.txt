
# Combined home ranges determined by convex hulls; start with the convex hull 
# of the first bear add those of the others by spatial union. Note that 
# st_convex_hull needs a MULTIPOINT geometry to compute a single convex hull
# st_union is used for creating the expected input geometry type

chull <- st_convex_hull(st_union(mating2009[mating2009$Name == bearnames[1],]))

for (name in bearnames[2:length(bearnames)]){
  tmp_hull <- st_convex_hull(st_union(mating2009[mating2009$Name == name,]))
  chull <- st_union(chull, tmp_hull)
  rm(tmp_hull)
}
