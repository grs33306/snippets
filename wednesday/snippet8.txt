# ***** Back to the bears // Potential path area affected by slope

DEM <- rast("DEM.tif") # {terra}
# Read August 2007 data
data2007 <- read.csv("August2007.txt", header = T)

# SUBSET: select 2 records from Koski trajectory and DEM covering that bit
Koski2007ab <- subset(data2007, PubName == "Koski (2310)")[c(100, 200),]
