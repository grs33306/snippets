
# 3D disdplay of triangulated surfaces {rgl}
open3d(windowRect=c(50,50,600,600))

# set the axes {rgl}
plot3d(NULL, xlim=c(-5,5),ylim=c(-5,5),zlim=c(min(t1,t3), max(t2,t4)),
       xlab="x", ylab = "y", zlab="t")

# add the surfaces {rgl}
plot3d(dxyz1, col = "blue", add = T, aspect=F)
plot3d(dxyz2, col = "light blue", add = T)
plot3d(dxyz3, col = "red", add = T)
plot3d(dxyz4, col = "pink", add = T)
