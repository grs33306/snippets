# plot (&spin) them in two colours
open3d(windowRect=c(50,50,600,600))
plot3d(bead1, type="p", col="red", xlim=c(-5,5),ylim=c(-5,5),zlim=c(min(t1,t3), max(t2,t4)))
lines3d(c(x1,x2),c(y1,y2), c(t1,t2), col="red")
points3d(bead2, col="blue")
lines3d(c(x3,x4),c(y3,y4), c(t3,t4), col="blue")
play3d(spin3d(axis=c(0,0,1), rpm=5), duration=15)
play3d(spin3d(axis=c(0,1,0), rpm=5), duration=15)
play3d(spin3d(axis=c(1,0,0), rpm=5), duration=15)