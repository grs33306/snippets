library(rgl)
rm(list=ls())
setwd("D:/Alibi")

# ++++ SOME DATA ++++
t1 = 0.0; x1 = -1.0; y1 = -1.0; t2 = 1.0; x2 = -1.5; y2 = -1.0; v1 = 4.0; t3 = 0.0; x3 = 1.0; y3 = 1.0; 
t4 = 2.0; x4 = 1.0; y4 = 1.0; v2 = 2.5


# define dense space-time grid
xyt <- expand.grid(x=seq(-5,5,length=100), y=seq(-5,5,length=100), 
                   t=(seq(min(t1,t3), max(t2,t4),length=100)))